
.. ifconfig:: releaselevel == 'live'

   DAVx⁵ Manual
   ============

   .. image:: images/davx5-logo.svg
      :width: 25%

.. ifconfig:: releaselevel == 'draft'

   DAVx⁵ Manual – *DRAFT*
   ======================

   .. warning::

      This is a working draft. See the `live manual <https://www.davx5.com/manual>`_.
   
      `Draft compiled from master branch <https://bitfireat.gitlab.io/davx5-manual/>`_


This is the manual for `DAVx⁵ <https://www.davx5.com>`_, an CalDAV/CardDAV client and sync app for Android.

The `source code of this manual is available <https://gitlab.com/bitfireAT/davx5-manual>`_.
Please `send merge requests <https://gitlab.com/bitfireAT/davx5-manual/blob/master/CONTRIBUTING.md>`_ to make it better!


.. seealso::

   For more information about DAVx⁵, see the `DAVx⁵ Web site <https://www.davx5.com>`_. Especially have a look at the
   `tested services <https://www.davx5.com/tested-with>`_ and the respective subpages, the
   `Frequently Asked Questions (FAQ) <https://www.davx5.com/faq>`_, and
   `our forums <https://www.davx5.com/forums>`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   accounts_collections
   settings
   technical_information
   managed_davx5


.. raw:: html

   <p>This work is licensed under a <a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>

